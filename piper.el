﻿;;; PIPER --- Use an Emacs buffer for a pipe contents between shell commands
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2019, Howard X. Abrams, all rights reserved.
;; Created:  2 September 2019
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;    To move away from the shell and do it like an Emacsian, means
;;    coming up with some data (either from a file or the output from an
;;    executable) and storing that data in a buffer. We would then
;;    *operate* on that buffer's contents with functions as if we were
;;    piping the data from one executable to the next.
;;
;;    This file contains functions to help with this paradigm.
;;
;;    To begin, call `piper' and give it a shell command to run. When it
;;    has finished, it will bring up a buffer with the standard output
;;    as the contents. You can then interact with the output using a
;;    hydra of commands, like `keep-lines' that is like `grep',
;;    `change-columns' that is like `cut', etc. Don't see what you need?
;;
;;    Just hit the `|' key and start another shell command, which will
;;    get the contents of the buffer in its standard input, and will
;;    replace the buffer with the output from that shell command...just
;;    like a pipe. Only you get to see the results of the pipe command
;;    during each step, and if it doesn't work, simply undo.
;;
;; THREE PRIMARY INTERFACES:
;;
;;   piper :: Runs a command in local directory and start the piper interface on results
;;   piper-other :: Same as `piper' except it prompts for a directory
;;   piper-remote :: Same as above, but it also prompts for a remote host,
;;                   where the command is executed.
;;
;;   Another approach is to call `piper' from within an eshell, where
;;   the command given to it are executed, but the results are placed
;;   in a buffer and the `piper-user-interface' started is.
;;
;; INTERESTING CHALLENGES:
;;
;;    - Shells assume parameter separation with spaces
;;    - Files and other parameters can have spaces as well
;;    - Want to minimize knowing when to put quotes around things
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(require 'cl)
(require 'dash)
(require 'hydra)
(require 's)

(defvar piper--delimiters '("," "|" "[ \t]"))
(defvar piper--column-fields '())
(defvar piper--for-loop-field-label (rx "{}"))

(defvar piper--host-history '())
(defvar piper--directory-history '())
(defvar piper--command-history '())

;; This code is primarily "interactive". Any code specific for "automated" scripts go:
(require 'piper-script)

;; Common code between the "interactive" and "automated" code files go:
(require 'piper-operations)


(defun piper (command)
  "Runs a shell command (with a history), with results always in
a displayed buffer. The piper UI is automatically invoked in
order to further process the results.

To run the command with root privileges, use `piper-other' with a C-u prefix.
See also `piper-user-interface'."
  (interactive
   (list
    (completing-read "Command: " piper--command-history nil nil nil
                     'piper--command-history (first piper--command-history))))
  (-let (((name program parameters outbuf) (piper--command-parts command)))
    (piper--run-process name program parameters outbuf default-directory t t '() t)))

(defun piper-other (prefix directory command)
  "Calls `piper', but first asks and changes to DIRECTORY before
running the shell COMMAND.

Calling with a prefix (C-u) executes command with sudo.
See also `piper-user-interface'."
  (interactive
   (list
    (prefix-numeric-value current-prefix-arg)
    (read-directory-name "Directory: ")
    (completing-read "Command: " piper--command-history nil nil nil
                     'piper--command-history (first piper--command-history))))
  (let ((default-directory
          (if (= prefix 4)
              (format "/sudo:localhost:%s" directory)
            directory)))
    (piper command)))

(defun piper-remote (prefix remote-host directory command)
  "Calls `piper', but asks for a REMOTE-HOST, in a particular
DIRECTORY, and a shell COMMAND. Each prompt has a unique history.
Like `piper', the results are always displayed in a buffer, and
the Piper UI is started for processing the results (all of which
are executed on the remote system using Tramp).

Calling with a prefix (C-u) calls the remote command with sudo.
See `piper-user-interface'."
  (interactive
   (list
    (prefix-numeric-value current-prefix-arg)
    (completing-read "Hostname: " piper--host-history nil nil nil
                     'piper--host-history (first piper--host-history))
    (completing-read "Directory: " piper--directory-history nil nil nil
                     'piper--directory-history (first piper--directory-history))
    (completing-read "Command: " piper--command-history nil nil nil
                     'piper--command-history (first piper--command-history))))

  (let* ((history-tuple (assoc remote-host piper--host-history))
         (host-address  (if (and history-tuple (stringp (cdr history-tuple)))
                            (cdr history-tuple)
                          remote-host))
         (remote-dir
          (if (= prefix 4)
               (format "/ssh:%s|sudo:%s:%s" host-address host-address directory)
             (format "/ssh:%s:%s" host-address directory))))
    (piper-other remote-dir command)))

(defun eshell/piper (&rest command)
  "Interface to call the `piper-interface' on the output from a
command. In the eshell, the command can follow the `piper'keyword."
  (interactive "s")
  (when command
    (->> command
         ;; eshell helps out by converting parameters that look like numbers to
         ;; numbers, but we need everything as a string in order to join them:
         (--map (if (stringp it) it (prin1-to-string it)))
         (s-join " ")
         piper)))


;; HELPER SUB FUNCTIONS

(defun piper--command-parts (command)
  "Returns a list of the following aspects of the shell COMMAND:

    - name (the name program without parent directory)
    - program
    - parameters
    - buffer name to use"
  ;; Just in case a nil shows up at this point:
  (if (s-blank-str? command)
      '("sh" "no-program" '("-c" "echo") "*no-program*")

    (-let* ((parts (->> command
                      s-trim
                      (s-split (rx (+ blank)))))
            (program (first parts))
            (progdir (split-string program "/"))
            (script  (or (first (last progdir)) program))
            ((name ext) (split-string script "\\."))
            (buffer-name (format "*%s*" name)))
      (list name program (rest parts) buffer-name))))

(s-blank-str? nil)
(piper--command-parts nil)


(defun piper--process-name-from-shell (command)
  "Return a good name for a process (and its buffer) based on the
COMMAND given."
  (first (piper--command-parts command)))


(defun piper--run-process (name program &optional parameters buffer
                                directory clear-buffer user-interface
                                repeat-parameters debug)
  "Executes a PROGRAM with PARAMETERS in a DIRECTORY the piper way.

If CLEAR-BUFFER is non-nil, the buffer created based on NAME is
erased first.

If USER-INTERFACE is non-nil, when the process completes
asynchronously, not only is it displayed to the user, but the
`piper-user-interface' is called."
  (when (null buffer) (setq buffer (format "*%s*" name)))
  (when (null directory) (setq directory default-directory))
  (when (null clear-buffer) (setq clear-buffer t))

  ;; Reusing a buffer due to the name, let's get rid of it at this
  ;; point, and use `bury-buffer' later when we want it to go away.
  (when clear-buffer
    (ignore-errors (kill-buffer buffer)))

  (let* ((default-directory directory)
         ;; To simulate what a shell does (but still allowing us to ignore spaces),
         ;; we'll see if the shell could (should) expand on the results automatically:
         (expanded-params (->> parameters
                             (-map #'piper-script-get-files)
                             -flatten))
         (properties `(name ,name
                       program ,program
                       debug ,debug
                       repeat-parameters ,repeat-parameters
                       directory ,directory
                       user-interface ,user-interface))

         (proc   (apply #'start-file-process name buffer program expanded-params)))
    (set-process-plist proc properties)
    (set-process-sentinel proc 'piper--process-command)

    (when debug
      (message "piper: Executing '%s %s' in: %s" program expanded-params directory))))

(defun piper--repeat-process (process)
  "If a process has a `repeat-parameters' list, then we get the
process properties, and start another process, but this time
running it with just the first element on the list (which is a
list of parameters), and settings its process properties with the
rest of that list. Get it?"
  (let* ((name    (process-get process 'name))
         (buffer  (process-buffer process))
         (program (process-get process 'program))
         (debug   (process-get process 'debug))
         (repeats (process-get process 'repeat-parameters))
         (dir     (process-get process 'directory))
         (ui      (process-get process 'user-interface))

         ;; Create a new properties list for the next process iteration:
         (properties  `(name ,name
                        program ,program
                        debug ,debug
                        repeat-parameters ,(rest repeats)
                        directory ,dir
                        user-interface ,ui))

         (expanded-params (->> (first repeats)
                             (-map #'piper-script-get-files)
                               -flatten))
         (params  )
         ;; Run the same program, but with the start of the list as its parameters:
         (proc   (apply #'start-process name buffer program expanded-params)))

    (set-process-plist proc properties)
    (set-process-sentinel proc 'piper--process-command)))

(defun piper--process-command (process event)
  "Display buffer associated with PROCESS as well as call the
`piper-user-interface' is the process was executed successfully."
  (let ((status  (process-status process))
        (excode  (process-exit-status process))
        (repeat  (process-get process 'repeat-parameters))
        (ui-p    (process-get process 'user-interface)))

    (if repeat
        (piper--repeat-process process)

      (piper--show-process-buffer process event)
      (when (and ui-p (eq status 'exit) (= excode 0))
        (piper-user-interface)))))

(defun piper--show-process-buffer (process event)
  "Display the buffer associated with PROCESS (called via
sentinel) as well as append event status."
  (let ((bufname (process-buffer process))
        (defdir  (process-get process 'directory))
        (status  (process-status process))
        (excode  (process-exit-status process)))
    (switch-to-buffer bufname)
    (make-variable-buffer-local 'default-directory)
    (setq default-directory defdir)

    (when (> excode 0)
      (goto-char (point-max))
      (insert (format "\n\n** Exit Code: %d" excode)))))


(defun piper-user-interface ()
  "Displays a menu showing a collection of functions to call to
transform a buffer. The functions are focused on calling
executables on a command line.

Note: This interface would be more useful if we had full
movement, for instance, Capital letters manipulated the buffer in
shell-like ways, but lower case letter behaved like Evil mode,
and all the normal Emacs worked as well (C-n for moving down a
line, etc). This function has _some_ of those, but never enough."
  (interactive)
  (hydra-piper/body))

(defhydra hydra-piper (global-map "C-c !" :hint nil)
  "
    ^Move^        ^Execute^         ^Lines^             ^Ordering^        ^Table/Characters^
 ^^^^^^^----------------------------------------------------------------------------------------
    _k_: Up       _|_: Pipe      _d_/_D_: Delete/Regexp  _s_: Sort Lines    _r_: Replace
    _j_: Down     _!_: Send To     _K_: Keep           _S_: Reverse Sort  _R_: Replace All
    _h_: Left     _x_: Use Xargs   _J_: Join Lines     _U_: Uniquify      _c_: Change Column(s)
    _l_: Right    _f_: Each line
  _/_/_?_: Search                         %s(piper--count-words)
  _g_/_G_: Top/Bottom"
  ("<prior>" scroll-up-command) ("<next>" scroll-down-command)
  ("C-b" scroll-up-command)     ("C-f" scroll-down-command)
  ("k" evil-previous-line)      ("<up>" evil-previous-line)
  ("j" evil-next-line)          ("<down>" evil-next-line)
  ("h" evil-backward-char)      ("<left>" evil-backward-char)
  ("l" evil-forward-char)       ("<right>" evil-forward-char)
  ("/" evil-search-forward)     ("?" evil-search-backward)
  ("g" (goto-char (point-min))) ("G" (goto-char (point-max)))

  ("|" piper-pipe)
  ("!" piper-shell :color blue)
  ("x" piper-xargs-command :color blue) ("#" piper-xargs-command :color blue)
  ("f" piper-command-each-line :color blue)

  ("d" kill-whole-line)
  ("D" piper-script-grep-v)
  ("K" piper-script-grep)
  ("J" piper-join-lines)
  ("r" piper-script-query-replace)
  ("R" piper-script-replace)

  ("s" piper-script-sort)
  ("S" piper-script-sort-r)
  ("U" piper-script-uniq)

  ("c" piper-keep-columns)

  ("v" piper-toggle-select)
  ("u" undo-tree-undo "undo")
  ("y" piper-script-to-clipboard "copy" :color blue)
  ("Y" piper-copy-and-finish "copy & close" :color blue)
  ("Q" bury-buffer "exit" :color blue)
  ("q" nil "quit"))

(defun piper--count-words ()
  (format "(lines:%d words:%d chars:%d )"
          (count-lines (point-min) (point-max))
          (count-words (point-min) (point-max))
          (- (point-max) (point-min))))

;; SHELL-LIKE HELPERS

(defun piper-pipe (command)
  "Replaces the contents of the buffer, or the contents of the
selected region, with the output from running an external
executable, COMMAND.

This is just a wrappers around `shell-command-on-region', but
with our special command history."
  (interactive
   (list
    (completing-read "Command: " piper--command-history nil nil nil
                     'piper--command-history (first piper--command-history))))
  (save-excursion
    (save-restriction
      (when (region-active-p)
        (narrow-to-region (region-beginning) (region-end)))
      (shell-command-on-region (point-min) (point-max) command nil t))))

(defun piper-shell (command)
  "Run COMMAND with the contents of the buffer, or the contents
of the selected region (if selected). Results displayed in a new
buffer, but the UI is not automatically started."
  (interactive
   (list
    (completing-read "Command: " piper--command-history nil nil nil
                     'piper--command-history (first piper--command-history))))
  (-let (((name program parameters outbuf) (piper--command-parts command)))
    (piper--run-process name program parameters outbuf default-directory t nil)))

(defun piper-xargs-command (command)
  "Sends the contents of the current buffer (or region is active)
to a shell COMMAND as _arguments_ similar to the `xargs' executable."
  (interactive
   (list
    (completing-read "Command: " piper--command-history nil nil nil
                     'piper--command-history (first piper--command-history))))
  (save-restriction
    (when (region-active-p)
      (narrow-to-region (region-beginning) (region-end)))

    (-let* ((lines (split-string (buffer-string) "\n" t "[[:space:]]+"))
            ((name program parameters outbuf) (piper--command-parts command))
            (all-parms (append parameters lines)))

      ;; Process is started asynchronously and will "pop up" when finished:
      (piper--run-process name program all-parms outbuf default-directory t nil))))

(defun piper--replace-braces (lst substitution)
  "Looks through the list, LST, for a string entry of `{}' (like
the `find' program), and returns a new list, where that element
has been replaced with the string, SUBSTITUTION."
  (--map (replace-regexp-in-string piper--for-loop-field-label substitution it) lst))

(defun piper--replace-or-append (lst parameter)
  "Either appends PARAMETER to the end of LST, or searches for
entries of `{}' and replaces those textual strings with
PARAMETER."
  (let ((replaced-version (piper--replace-braces lst parameter)))
    (if (equal lst replaced-version)
        (append lst (list parameter))
      replaced-version)))

(defun piper-command-each-line (prefix command)
  "Call the COMMAND repeatedly for each line in the current
buffer. The output from the calls should get appended to some
buffer, right?"
  (interactive
   (list
    (prefix-numeric-value current-prefix-arg)
    (completing-read "Command: " piper--command-history nil nil nil
                     'piper--command-history (first piper--command-history))))

  (-let* (((name program parameters outbuf) (piper--command-parts command))
          (line-list (save-restriction
                      (when (region-active-p)
                        (narrow-to-region (region-beginning) (region-end)))
                      (split-string (buffer-string) "\n" t "[[:blank:]]")))

          ;; If {} is entered, substitute it, otherwise, append it:
          (arg-list (--map (piper--replace-or-append parameters it) line-list)))

    (piper--run-process name program (first arg-list) outbuf
                        default-directory t nil (rest arg-list)
                        ;; If given a prefix, pass `t' for debug
                        (= prefix 4))))

(defun piper-copy-and-finish ()
  "The proper way to finish your work on the buffer is to copy
the entire contents of the buffer to the clipboard, and then bury
the buffer to work on what you were doing."
  (interactive)
  (piper-script-to-clipboard)
  (bury-buffer))

(defun piper-join-lines (delimiter)
  "Joins all lines in the buffer, optionally separated by a
delimiter string."
  (interactive "sDelimiter: ")
  (save-excursion
    (save-restriction
      (when (region-active-p)
        (narrow-to-region (region-beginning) (region-end)))

      (goto-char (point-min))
      (while (search-forward "\n" nil t)
        (replace-match delimiter)))))


;; --------- Table Functions -----------

;; Since the `cut' command allows us to specify a complicated collection
;; of numbers, like 1,3-6,8- ... we need a way to convert a string of
;; this style into an actual list of numbers:

(defun piper--range-of-numbers (str)
  "Convert a string, STR, that specifies a range of numbers into
a list of integers."
  (if (s-blank-str? str)
      '()

    (let* ((initial-dash-re (rx bol (0+ (any space)) "-" (0+ (any space))))
           (embedded-dash-re (rx (0+ (any space)) "-" (0+ (any space))))
           (space-comma-re   (rx (0+ (any space))
                                 (any space ",")     ; Need at least one space or comma
                                 (0+ (any space))))
           (number-range-re  (rx (group (1+ (any digit)))
                                 (0+ (any blank))
                                 "-"
                                 (0+ (any blank))
                                 (group (1+ (any digit)))))

           ;; Start with a dash? Let's have that as a range from 1:
           (initial-one (replace-regexp-in-string initial-dash-re "1-" str))

           ;; Since we can separate on spaces, make sure dashes don't have spaces:
           (spaceless-dash (replace-regexp-in-string embedded-dash-re "-" initial-one)))

      (-flatten
       (-map (lambda (n)
               (if (not (string-match number-range-re n))
                   (string-to-number n)
                 ;; Got a range, convert to a sequence:
                 (number-sequence (string-to-number (match-string 1 n))
                                  (string-to-number (match-string 2 n)))))
             (split-string spaceless-dash space-comma-re t))))))

;; ------------------------------------------------
;;  A replacement for the `cut' cli program...

(defun piper--column-lines-for-line (line delimiter fields &optional trim)
  "Separates LINE into a list of strings delimited with
DELIMITER. Returns elements specified by FIELDS, which could be a
single integer number, a collection of columns (separated by
commas), or a range with two numbers separated by a dash."
  (let* ((trim-rx (when trim (rx (1+ blank))))
         (elements (split-string line delimiter nil trim-rx)))
    (-map (lambda (n)
            (nth-value (1- n) elements))
          fields)))

(defun piper--keep-columns (line delimiter fields &optional trim)
  "Return LINE where only the text separated by DELIMITER characters,
which we'll call columns, is made by only keeping those columns specified
by FIELDS (and re-separated by the delimiter string)."
  (if (= 0 (length fields))
      ""    ; Short-circuit the process if no fields are given

    (when (= 1 (length fields))
      (setq trim t))

    (let ((columns (piper--column-lines-for-line line delimiter fields trim)))
      (s-join delimiter columns))))

(defun piper--keep-columns-buffer (delimiter fields trim)
  "Replaces each line with a new line of contents given by
calling `piper--keep-columns' with the parameters given."
  (while (< (line-end-position) (point-max))
    (let* ((begin-line    (line-beginning-position))
           (end-of-line   (line-end-position))
           (line-contents (buffer-substring-no-properties begin-line end-of-line))
           (new-contents  (piper--keep-columns line-contents delimiter fields trim)))
      (delete-region begin-line end-of-line)
      (insert new-contents)
      (goto-char (line-end-position 2)))))

(defun piper-keep-columns (delimiter fields &optional trim)
  "Replaces contents of buffer with a subset where each line is a
particular field separated by a delimiter. This is similar to
the `cut' command, but without all the features...yet."
  (interactive (list
                (read-string "Column delimiter: " nil 'piper--delimiters)
                (read-string "Column number(s) to Keep: " nil 'piper--column-fields)))
  (when (equal current-prefix-arg '(4))
    (setq trim t))

  (let ((field-choices (piper--range-of-numbers fields)))
    (save-excursion
      (goto-char (point-min))
      (piper--keep-columns-buffer delimiter field-choices trim))))

(defun piper-flush-columns (delimiter fields)
  "Replaces contents of buffer with a subset where each line is NOT a
particular field separated by a delimiter. This is the inverse to
the `cut' command."
  (interactive "sDelimiter: \nsColumn Numbers(s): ")
  (message "piper: This method is currently unimplemented."))

(provide 'piper)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; piper.el ends here
